from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"AUTHORIZATION": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    picture_data = json.loads(requests.get(url, headers=headers)._content)
    try:
        picture_url = picture_data["photos"][0]["src"]["original"]
        return picture_url
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    geo_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    # geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    # geocode_data = json.loads(requests.get(geo_url, params=geo_params)._content)
    geocode_data = (requests.get(geo_url, params=geo_params).json())
    try:
        latitude = geocode_data[0]["lat"]
        longitude = geocode_data[0]["lon"]
    except (KeyError, IndexError):
        return None
    url2 = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"
    weather_data = json.loads(requests.get(url2)._content)
    try:
        temp = (((weather_data["main"]["temp"] - 273.15) * 9) / 5) + 32
        description = weather_data["weather"][0]["description"]
    except (KeyError, IndexError):
        return None
    return {
        "temp": temp,
        "description": description,
    }
